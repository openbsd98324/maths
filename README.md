# maths




## bc and nlinecalc  

A simple calculation: 

    "(-14)*(-3)-2*((-2)-(-16)*4)" 


````
   echo "(-14)*(-3)-2*((-2)-(-16)*4)"  | bc 
-82
````

## FLTK FLCALC 

Library FLTK 


![](medias/1696890671-screenshot.png)


## Language C (GCC compiler)

File: 
calc-exr1.c

![](medias/1696891139-screenshot.png)

![](medias/1696891152-screenshot.png)


## Qalculate-gtk

Running on Linux PI.

![](medias/1696892603-screenshot.png)



## Tilem 

````
  cd ; tilem2 -r .ti84plus.rom >
````



![](medias/1696874461-screenshot.png) 



## Mathomatic 

Mathomatic on debian (rpi3b).

![](medias/1696875396-screenshot.png)


## HP48 (x48) 

x48 on debian (rpi3b).
Raspbian 
uname -r 
 Linux retropie 4.14.98-v7+ 

![](medias/1696875696-screenshot.png)

![](medias/1696876015-screenshot.png)

![](medias/1696876041-screenshot.png)

![](medias/1696876073-screenshot.png)






## TI85 Emu on Raspberry pi  
 

````
  tilem2 --rom t85v100.bin
````


![](medias/1696877853-screenshot.png)



## TI86 using tilem emulation on raspberry pi 


![](medias/1696879823-screenshot.png)



## TIEMU (ti92) emulation on raspberry pi 


![](medias/1696879276-screenshot.png)




## On Phone Android, TI89 Emulation 

APK Package for Android:

https://gitlab.com/openbsd98324/maths/-/raw/main/pub/contrib/android/ti89/Graph_89_-__with_TI84_support__1.1.3c_Apkpure.apk




![](pub/contrib/android/ti89/Screenshot_20231021-204904.jpg)





## On Phone Android, TI84SE (Ti 84 Silver edition)


APK Package for Android:

https://gitlab.com/openbsd98324/maths/-/raw/main/pub/contrib/android/ti84se/Wabbitemu_1.06.6_Apkpure.apk



![](pub/contrib/android/ti84se/Screenshot_20231021-210104_Wabbitemu.jpg)




# DOSBOX (running on great number of platforms)


MatLab 4.x

![](medias/1697917660-screenshot.png)



# TiEmu on Linux (Devuan) running Ti89 


````
  tiemu ti89.rom
````

![](medias/1697947850-screenshot.png)




# TiEmu on Linux (Devuan) running Ti92

![](medias/1697948490-screenshot.png)

````
 tiemu ti92p.rom
````



# Gnuplot on Linux (Devuan) 

````
 gnuplot 
````

![](medias/1697949079-screenshot.png)

## Solution 

Exr 1: 

 The solution is minus 82 


![](medias/20231021_212225.jpg)


